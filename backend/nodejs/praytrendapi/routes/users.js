import bcrypt from 'bcrypt';
import isEmpty from 'lodash/isEmpty';
import commonValidations from '../validations/signup';
import User from '../models/user';

function validateInput(data, otherValidations) {
  let { errors } = otherValidations(data);

  return User.query({
    where: { email: data.email },
    orWhere: { username: data.username }
  }).fetch().then(user => {
    if (user) {
      if (user.get('username') === data.username) {
        errors.username = 'There is user with such username';
      }
      if (user.get('email') === data.email) {
        errors.email = 'There is user with such email';
      }
    }

    return {
      errors,
      isValid: isEmpty(errors)
    };
  })

}

exports.register = function (req, res) {
  validateInput(req.body, commonValidations).then(({ errors, isValid }) => {
    if (isValid) {
      const { username,
      email,
      password,
      first_name,
      last_name } = req.body;
      const password_digest = bcrypt.hashSync(password, 10);

      User.forge({
        username, email, password_digest, first_name, last_name
      }, { hasTimestamps: true }).save()
        .then(user => res.json({ success: true }))
        .catch(err => res.status(500).json({ error: err }));

    } else {
      res.status(400).json(errors);
    }
  });

};

/*
let router = express.Router();
exports.register = function (req, res) {
  // console.log("req",req.body);
  var today = new Date()
  bcrypt.hash(req.body.password, 5, function (err, bcryptedPassword) {
    var users = {
      'first_name': req.body.first_name,
      'last_name': req.body.last_name,
      'email': req.body.email,
      'password': bcryptedPassword,
      'created': today,
      'modified': today
    }
    connection.query('INSERT INTO users SET ?', users, function (error, results, fields) {
      if (error) {
        console.log('error ocurred', error)
        res.send({
          'code': 400,
          'failed': 'error ocurred'
        })
      } else {
        console.log('The solution is: ', results)
        res.send({
          'code': 200,
          'success': 'user registered sucessfully'
        })
      }
    })
  })
}
*/

// https://developers.facebook.com/apps/109437546494827/dashboard/
// https://www.raymondcamden.com/2017/02/08/using-social-login-with-passport-and-node/
/*
const FACEBOOK_APP_ID = '109437546494827'
const FACEBOOK_APP_SECRET = 'a867d73a19c6dd7c7eb3c55816299859'
const FACEBOOK_CALLBACK_URL = "http://localhost:5000/auth/facebook/callback"

passport.use(new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: FACEBOOK_CALLBACK_URL
  },
  function(accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ facebookId: profile.id }, function (err, user) {
      return cb(err, user);
    });
  }
));
*/
