import bcrypt from 'bcrypt';
import prayerviewer from '../models/prayerviewer';
import authenticate from '../middlewares/authenticate';

exports.getPrayers = function (req, res) {

  prayerviewer.query({
  }).fetchAll().then(prayers => {
    return res.send({
        'code': 200,
        'data': prayers,
        'message': 'Successfully returning prayers'
      });
  }).catch(function (e) {
   return res.send({
      'code': 400,
      'data': prayers,
      'message': 'User Error'
    }); 
  });
}

/*
  TODO Please check if the updated value is lower than the currently stored value

  happens on the frontend:
  Can only update a mounted or mounting component. Please check the code for the Icon component.
*/
exports.updatePrayer = function (req, res) {
  const { location, latitude, longitude } = req.body;
  let currentCount = 0;

  // Need to have results push to both from API and websocket
  // Or... use redux instead to update values.
  prayerviewer.query({
    where: { location: location
    , latitude: latitude
    , longitude: longitude
    }
  }).fetch().then(prayerEvent => {
    if (!prayerEvent) {
      res.status(500).json({ error: 'Marker does not exist' })
    } else {
      currentCount = prayerEvent.attributes.prayer_count + 1;
    }
  })
  .then(function () {
    prayerviewer.forge()
    .where({location: location
    , latitude: latitude
    , longitude: longitude
    }).save({location: location
      , latitude: latitude
      , longitude: longitude
      , prayer_count: currentCount}
    ,{method:"update"})
    .then(function (updatedPrayer) {
      io.emit('FromAPI', {message: 'filler data1', data: updatedPrayer})
        res.json({
          'code': 200,
          'success': 'Added prayer or good vibe towards location.',
          'data': updatedPrayer
        })
    })
    .catch(err => res.status(500).json({ error: err }))
  });
}

exports.addPrayer = function (req, res) {
  const { location, latitude, longitude, prayer_count } = req.body;
  
  prayerviewer.forge({
    location, latitude, longitude, prayer_count
  }, { hasTimestamps: true }).save()
    .then(addPrayer => res.json({ success: true, message: "Added new marker." }))
    .catch(err => res.status(500).json({ error: err }));
}
