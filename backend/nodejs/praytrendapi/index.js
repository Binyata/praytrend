/*
TODO:
1. http to https https://woorkup.com/http-to-https/
2. ws to wss https://stackoverflow.com/questions/18135159/what-do-i-have-to-take-care-of-when-switching-from-ws-to-wss 
3. more proper authentication/authorization
4. make sure clicks are always going a value up
5. might need to add nginx?
6. Need api key to track usage

*/

/*

New install:

remove node_modules folder
npm install
npm install -g knex // knex is the migrations
knex migrate:latest || knex migrate:rollback
npm install bookshelf // handles models and uses knex
npm install -g nodemon // keep nodejs running while coding

*/

import express from 'express';
import http from 'http';
import socket from 'socket.io';
import bodyParser from 'body-parser';
import users from './routes/users';
import auth from './routes/auth';
import prayer from './routes/prayer';
import authenticate from './middlewares/authenticate';


let app = express();

// route to handle user registration
let router = express.Router()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS')
  res.header('Access-Control-Allow-Credentials', 'true')
  next()
})

router.post('/users/register', users.register);
router.post('/auth/login', auth.login);
router.post('/prayers/addPrayer', authenticate, prayer.addPrayer);
router.put('/prayers/updatePrayer', authenticate, prayer.updatePrayer);
router.get('/prayers', prayer.getPrayers);
app.use(router);

// Web Socket
const server = http.createServer(app)
var io = socket(server)
global.io = io
io.on('connection', socket => {
  console.log('New client connected, total: ', io.engine.clientsCount), setInterval(
    () => null,
    15000
  )
  socket.on('disconnect', () => console.log('Client disconnected'))
})

// app.get('/facebook-login', passport.authenticate('facebook'));
// app.get('/facebook-token', passport.authenticate('facebook', { failureRedirect: '/error' }),
//   function(req, res){
//     res.send('Logged In.');
//   });
// app.get('/error', function(req, res){
//   res.send('An error has occured.');
//   });
// app.use(passport.initialize());
// app.use(passport.session());
server.listen(4001, () => console.log(`Listening on port 4001 -- WebSocket`))
app.listen(5000, () => console.log(`Listening on port 5000 --API`));







// var express    = require("express");
// var login = require('./routes/loginroutes');
// var appRoute = require('./routes/approutes');
// var bodyParser = require('body-parser');

// var app = express();
// var server = require('http').createServer(app);
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());

// app.use(function(req, res, next) {
//     //res.header("Access-Control-Allow-Origin", "http://localhost:3000");
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.header("Access-Control-Allow-Methods", "POST");
//     res.header("Access-Control-Allow-Methods", "GET");
//     next();
// });
// var router = express.Router();

// // test route
// router.get('/', function(req, res) {
//     res.json({ message: 'welcome to our upload module apis' });
// });

// //route to handle user registration
// router.post('/register',login.register);
// router.post('/login',login.login)
// router.get('/getPrayers', appRoute.getPrayers);
// router.post('/addPrayer', appRoute.addPrayer);
// router.post('/addNewMarker', appRoute.addNewMarker);
// app.use('/api', router);
// app.listen(5000);

// io.on("connection", socket => {
//   console.log("New client connected"), setInterval(
//     () => socket.emit("FromAPI", "something!!"),
//     10000
//   );
//   socket.on("disconnect", () => console.log("Client disconnected"));
// });

// io.on('connection', function(socket) {

// socket.on('event', function(data) {
//    console.log('A client sent us this dumb message:', data.message);
// });

//     io.emit('stats', { numClients: numClients });

//     console.log('Connected clients:', numClients);

//     socket.on('disconnect', function() {
//         numClients--;
//         io.emit('stats', { numClients: numClients });

//         console.log('Connected clients:', numClients);
//     });

// });
