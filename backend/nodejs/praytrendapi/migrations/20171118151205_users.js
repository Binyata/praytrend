
exports.up = function(knex, Promise) {
	return knex.schema.createTable('users', function(table) {
		table.increments();
		table.string('username').notNullable().unique();
		table.string('email').notNullable().unique();
		table.string('password_digest').notNullable();
		table.string('first_name').nullable();
		table.string('last_name').nullable();
		table.string('facebook_token').nullable();
		table.string('twitter_token').nullable();
		table.string('google_token').nullable();
		table.timestamps();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('users');
};
