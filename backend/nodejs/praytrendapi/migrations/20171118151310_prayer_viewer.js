
exports.up = function(knex, Promise) {
	return knex.schema.createTable('prayerviewer', function(table) {
		table.increments();
		table.string('location').notNullable();
		table.string('latitude').notNullable();
		table.string('longitude').notNullable();
		table.integer('prayer_count').nullable();
		table.timestamps();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable('prayerviewer');
};
