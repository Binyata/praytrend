FROM node:latest

COPY . /var/www/nodejs
WORKDIR /var/www/nodejs

RUN npm install bcrpyt
RUN npm install -g db-migrate
RUN npm install express
RUN npm install mysql
RUN npm install db-migrate-mysql
RUN npm install socket-io
RUN npm install

EXPOSE 5000

CMD ["node", "server.js"] 