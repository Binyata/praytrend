import React, { Component } from 'react';
import RegisterInterface from './RegisterInterface.js';

class LoginRegister extends Component {
	constructor(props){
		super(props);
		this.state= {
			username:'',
			password:'',
			loginscreen:[],
			loginmessage:'',
			buttonLabel:'Register',
			isLogin:true
		}
	}
	render() {
		return(
			<div>
				<button type="button">Login Maps Service</button>
	          	<button type="button" onClick={(event) => this.handleClick(event)}>Register Maps Service</button>
	          	<div>
	          	{this.state.loginscreen}
	          	</div>
          	</div>
		);
	}

	handleClick(event) {
		var loginscreen = [];
		loginscreen.push(<RegisterInterface />);
		this.setState({
			loginscreen:loginscreen,
			buttonLabel:"Login",
			isLogin:false
		})
	}
}

export default LoginRegister;