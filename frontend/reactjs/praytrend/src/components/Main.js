import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './login/LoginPage';
import SignupPage from './signup/SignupPage';
import PrayMain from './PrayMain';


const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={PrayMain}/>
      <Route path='/login' component={Login}/>
      <Route path='/register' component={SignupPage}/>
    </Switch>
  </main>
)

export default Main