import React, { Component } from 'react';
import { compose, withProps, withStateHandlers } from "recompose";
import axios from 'axios';
import SendPrayer from './SendPrayers.js';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from "react-google-maps";
import socketIOClient from "socket.io-client";

/*
TODO
1. need better frontend design
2. need to use icons in place of markers
3. need to have login/register interactive

Login/register on same page, but cannot increment marker counts until logged in

*/

const MapWithAMakredInfoWindow = compose(

  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyB22efrRzzjkEp5uXQ_yzkVjU9CWcpMUjY&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withStateHandlers(() => ({
    isOpen: false,
    index: -1
      }), {
    currentMarkerSelected: ({ isOpen, index }) => event => ({
      isOpen: !isOpen,
      index: event
    })
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={3}
    defaultCenter={{ lat: 31.751, lng: -95.740 }}
  >
  {console.log(props.markers)}
  {props.markers ? props.markers.map((item, index) => (
        <Marker
          key={index}
          position={{ lat: Number(item.latitude), lng: Number(item.longitude) }}
          onClick={(event) => props.currentMarkerSelected(index)}
        >
          {props.isOpen && props.index === index &&
            <InfoWindow 
              onCloseClick={(event) => props.currentMarkerSelected(index)}
            >
              <div>
              <SendPrayer 
                location={item.location}
                prayer_count={item.prayer_count}
                longitude={item.longitude}
                latitude={item.latitude}
              />
                <div style='width: 72px; height: 72px; border-radius: inherit; background-image: url("https://external-amt2-1.xx.fbcdn.net/safe_image.php?d=AQA0qROIkZOA3OZG&w=960&h=539&url=http%3A%2F%2Fthehill.com%2Fsites%2Fdefault%2Ffiles%2Fcnn.png&sx=0&sy=0&sw=980&sh=550&_nc_hash=AQBSHw2tVbiqY5RK"); background-position: center center;'></div>
              </div>
            </InfoWindow>
          }
        </Marker>
  )) : null}  
  </GoogleMap>
);

class PrayMain extends Component {

  constructor(props) {
    super();
    this.state = {
      loading: true,
      markers: {},
      response: false,
    }
  }

  componentDidMount() {
    let url = 'http://localhost:5000/prayers';
    let test;
    axios.get(url).then(response => {
      test = response.data.data;
      this.setState({
        markers: test,
        loading: false,
      });
      console.log(this.state);
    });

    const socket = socketIOClient("http://localhost:4001", {transports: ['websocket', 'polling', 'flashsocket']});
    socket.on("FromAPI", data => {
      if(data) {
        const test01 = this.state.markers;
        const something = test01.map((item, index) => 
          item.latitude === data.data.latitude && item.longitude === data.data.longitude ? data.data : item
        );
        this.setState({markers: something});
      }
    });
    console.log(this.state.markers);
  }

  render() {
    if(this.state.loading) {
      return <div>Loading...</div>
    }
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to PrayTrend.com</h1>
        </header>
        <MapWithAMakredInfoWindow 
          markers={this.state.markers}
          loaded={this.state.loading}
        />
      </div>
    );
  }
}
// AIzaSyB22efrRzzjkEp5uXQ_yzkVjU9CWcpMUjY
export default PrayMain;
