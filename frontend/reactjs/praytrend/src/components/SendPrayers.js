import React, { Component } from 'react';
import { updatePrayer } from '../actions/prayerAction';

class SendPrayer extends Component {

	componentDidMount() {
		console.log(this.props.markers);
	}
	render() {
		if(this.props.loading) {
			return <div>Loading...</div>
		}
		return (
			<div>
				<h1><b>EVENT</b></h1>
				<p>Location: {this.props.location}</p>
				<p>Latitude: {this.props.latitude}</p>
				<p>Longitude: {this.props.longitude}</p>
				<p>Prayer Count: {this.props.prayer_count}</p>
				<button type="button" 
				onClick={(event) => this.handleClick(event, this.props)}>
				Increment Count</button>
			</div>
		);
	}

	handleClick(event, one) {
		console.log(one);
		updatePrayer(one.location
				, one.latitude
				, one.longitude);
	}
}

export default SendPrayer;