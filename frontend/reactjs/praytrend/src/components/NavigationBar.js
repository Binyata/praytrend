import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../actions/authActions';
import PropTypes from 'prop-types';

import logo from '../logo.svg';

class NavigationBar extends React.Component {
	logout(e) {
	    e.preventDefault();
	    this.props.logout();
  	}
	render() {
		const { isAuthenticated } = this.props.auth;

		const userLinks = (
		<ul className="nav navbar-nav navbar-right">
		<li><a href="#" onClick={this.logout.bind(this)}>Logout</a></li>
		</ul>
		);

	    const guestLinks = (
	      <ul className="nav navbar-nav navbar-right">
	        <li><Link to="/login">Login</Link></li>
	      </ul>
	    );
		return (
			<nav className="navbar navbar-default">
				<div>
					<h2>Title/Logo</h2>
				</div>
				<ul>
					<li><Link to="/"><img src={logo} height="120" width="120" /></Link></li>
					<li><Link to="/register">Register</Link></li>
				</ul>
				<div className="collapse navbar-collapse">
	            	{ isAuthenticated ? userLinks : guestLinks }
	          </div>
			</nav>
		)
	}
}

NavigationBar.propTypes = {
  auth: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

export default connect(mapStateToProps, { logout })(NavigationBar);