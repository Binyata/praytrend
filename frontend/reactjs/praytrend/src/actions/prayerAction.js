import axios from 'axios';

// export function addPrayer(location, latitude, longitude, prayer_count) {
// 	let url = 'http://localhost:5000/api/addPrayer';
// 	let body = {
// 		"location":location,
// 		"latitude":lat,
// 		"longitude":long,
// 		"prayer_count":count
// 	}
// 	console.log('function addprayer');
// 	console.log(body);
// 	axios.post(url, body).then(response => {
//       console.log(response);
//     });
// }

export function updatePrayer(location, latitude, longitude) {
	let url = 'http://localhost:5000/prayers/updatePrayer';
	let body = {
		"location":location,
		"latitude":latitude,
		"longitude":longitude
	}
	console.log('function updateprayer');
	console.log(body);
	axios.put(url, body).then(response => {
      console.log(response);
    });
}