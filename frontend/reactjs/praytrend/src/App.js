import React, { Component } from 'react';

import Main from './components/Main.js';
import NavigationBar from './components/NavigationBar';
import Greetings from './components/Greetings';
/*
TODO
1. need better frontend design
2. need to use icons in place of markers
3. need to have login/register interactive

Login/register on same page, but cannot increment marker counts until logged in

use router and redux

*/


class App extends Component {

  constructor(props) {
    super();
  }

  componentDidMount() {

  }

  render() {
    return (
      <div className="container">
        <NavigationBar />
        <Main />
      </div>
    );
  }
}
// AIzaSyB22efrRzzjkEp5uXQ_yzkVjU9CWcpMUjY
export default App;


/*

      
      <Router>
      <Switch>
      <Route path="/" component={Greetings}/>
      <Route path="/login" component={Login}/>
      <Route path="/register" component={Register}/>
      </Switch>
      </Router> 
      */